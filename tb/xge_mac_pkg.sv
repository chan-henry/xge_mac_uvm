package xge_mac_pkg;

	import uvm_pkg::*;
	`include "uvm_macros.svh"

	`include "agents/wb/wb_transaction.svh"
	`include "ral/ral_xge_mac.svh"
	`include "ral/ral_sequences.svh"
	typedef uvm_reg_predictor#(wb_transaction) xge_mac_reg_predictor;
	`include "agents/wb/wb_sequences.svh"
	`include "agents/wb/wb_driver.svh"
	`include "agents/wb/wb_agent.svh"


	`include "agents/pkt/pkt_tx_transaction.svh"
	`include "agents/pkt/pkt_tx_sequences.svh"
	`include "agents/pkt/pkt_tx_driver.svh"
	`include "agents/pkt/pkt_tx_agent.svh"

	`include "agents/pkt/pkt_rx_transaction.svh"
	`include "agents/pkt/pkt_rx_sequencer.svh"
	`include "agents/pkt/pkt_rx_sequences.svh"
	`include "agents/pkt/pkt_rx_monitor.svh"
	`include "agents/pkt/pkt_rx_driver.svh"
	`include "agents/pkt/pkt_rx_agent.svh"

	`include "agents/xgmii/xgmii_transaction.svh"
	`include "agents/xgmii/xgmii_sequences.svh"
	`include "agents/xgmii/xgmii_driver.svh"
	`include "agents/xgmii/xgmii_agent.svh"

	`include "agents/reset/rst_driver.svh"

	`include "xge_mac_env.svh"
	`include "xge_mac_test.svh"

endpackage: xge_mac_pkg
