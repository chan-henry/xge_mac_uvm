`ifndef WB_ENV_SVH
`define WB_ENV_SVH

class xge_mac_env extends uvm_env;

	`uvm_component_utils(xge_mac_env)

	rst_driver		m_wb_rst_driver;
	rst_driver		m_rst_driver;
	wb_agent		m_wb_agent;
	pkt_tx_agent		m_pkt_tx_agent;
	pkt_rx_agent		m_pkt_rx_agent;
	xgmii_agent		m_xgmii_agent;
	xge_mac_reg_block	reg_block;
	xge_mac_reg_predictor	reg_predictor;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		m_wb_rst_driver	= rst_driver::type_id::create("m_wb_rst_driver", this);
		m_rst_driver	= rst_driver::type_id::create("m_rst_driver", this);
		m_wb_agent	= wb_agent::type_id::create("m_wb_agent", this);
		m_pkt_tx_agent	= pkt_tx_agent::type_id::create("m_pkt_tx_agent", this);
		m_pkt_rx_agent	= pkt_rx_agent::type_id::create("m_pkt_rx_agent", this);
		m_xgmii_agent	= xgmii_agent::type_id::create("m_xgmii_agent", this);
		reg_predictor	= xge_mac_reg_predictor::type_id::create("reg_predictor", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		reg_block.reg_map.set_sequencer(.sequencer(m_wb_agent.m_wb_sequencer), .adapter(m_wb_agent.reg_adapter));
		reg_block.reg_map.set_auto_predict(.on(0));
		reg_predictor.map	= reg_block.reg_map;
		reg_predictor.adapter	= m_wb_agent.reg_adapter;
	endfunction: connect_phase

endclass: xge_mac_env

`endif
