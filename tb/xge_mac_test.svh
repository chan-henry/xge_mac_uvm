`ifndef WB_TEST_SVH
`define WB_TEST_SVH

class xge_mac_test extends uvm_test;

	`uvm_component_utils(xge_mac_test)

	int run_count = 3;

	xge_mac_env		m_xge_mac_env;
	xge_mac_reg_block	reg_block;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		m_xge_mac_env = xge_mac_env::type_id::create(.name("m_xge_mac_env"), .parent(this));
		reg_block = xge_mac_reg_block::type_id::create(.name("reg_block"), .parent(this));
		reg_block.build();
		m_xge_mac_env.reg_block = reg_block;
	endfunction: build_phase

	virtual function void phase_ready_to_end(uvm_phase phase);
		super.phase_ready_to_end(phase);

		if(phase.get_imp() == uvm_shutdown_phase::get()) begin
			if(0 != run_count) begin
				phase.jump(uvm_pre_reset_phase::get());
				run_count--;
			end
		end

	endfunction: phase_ready_to_end

	task main_phase(uvm_phase phase);

		uvm_status_e		status;
		bit [63:0]		tmp64 = 64'h0;
		bit [31:0]		tmp32 = 32'h0;

		//wb_seq_reset		m_wb_seq_reset		= wb_seq_reset::type_id::create("m_wb_seq_reset");
		wb_seq_rand_idle	m_wb_seq_rand_idle	= wb_seq_rand_idle::type_id::create("m_wb_seq_rand_idle");
		wb_seq_rand_write	m_wb_seq_rand_write	= wb_seq_rand_write::type_id::create("m_wb_seq_rand_write");
		wb_seq_rand_read	m_wb_seq_rand_read	= wb_seq_rand_read::type_id::create("m_wb_seq_rand_read");
		wb_seq_write		m_wb_seq_write		= wb_seq_write::type_id::create("m_wb_seq_write");
		wb_seq_read		m_wb_seq_read		= wb_seq_read::type_id::create("m_wb_seq_read");
		wb_seq_idle		m_wb_seq_idle		= wb_seq_idle::type_id::create("m_wb_seq_idle");

		//pkt_tx_seq_rst		m_pkt_tx_seq_rst	= pkt_tx_seq_rst::type_id::create("m_pkt_tx_seq_rst");
		pkt_tx_seq_sop		m_pkt_tx_seq_sop	= pkt_tx_seq_sop::type_id::create("m_pkt_tx_seq_sop");
		pkt_tx_seq		m_pkt_tx_seq		= pkt_tx_seq::type_id::create("m_pkt_tx_seq");
		pkt_tx_seq_rand		m_pkt_tx_seq_rand	= pkt_tx_seq_rand::type_id::create("m_pkt_tx_seq_rand");
		pkt_tx_seq_eop		m_pkt_tx_seq_eop	= pkt_tx_seq_eop::type_id::create("m_pkt_tx_seq_eop");
		pkt_tx_seq_rand_idle	m_pkt_tx_seq_rand_idle	= pkt_tx_seq_rand_idle::type_id::create("m_pkt_tx_seq_rand_idle");

		pkt_rx_seq		m_pkt_rx_seq		= pkt_rx_seq::type_id::create("m_pkt_rx_seq");

		xgmii_seq_reset		m_xgmii_seq_reset	= xgmii_seq_reset::type_id::create("m_xgmii_seq_reset");
		xgmii_seq_sop		m_xgmii_seq_sop		= xgmii_seq_sop::type_id::create("m_xgmii_seq_sop");
		xgmii_seq		m_xgmii_seq		= xgmii_seq::type_id::create("m_xgmii_seq");
		xgmii_seq_rand		m_xgmii_seq_rand	= xgmii_seq_rand::type_id::create("m_xgmii_seq_rand");
		xgmii_seq_eop		m_xgmii_seq_eop		= xgmii_seq_eop::type_id::create("m_xgmii_seq_eop");

		xge_mac_reg_seq		m_xge_mac_reg_seq	= xge_mac_reg_seq::type_id::create("m_xge_mac_reg_seq");
		m_xge_mac_reg_seq.model = reg_block;

		phase.raise_objection(.obj(this), .description(get_name()));

		fork	// Sequences start in parallel, fork block waits until all are finished
		//m_wb_seq_reset.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);
		//m_pkt_tx_seq_rst.start(m_xge_mac_env.m_pkt_tx_agent.m_pkt_tx_sequencer);
		m_xgmii_seq_reset.start(m_xge_mac_env.m_xgmii_agent.m_xgmii_sequencer);
		join

		//m_wb_seq_rand_idle.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);

		fork
		m_pkt_rx_seq.start(m_xge_mac_env.m_pkt_rx_agent.m_pkt_rx_sequencer);
		join_none

		m_pkt_tx_seq_sop.data	= 64'h5555555555555555;
		m_pkt_tx_seq.data	= 64'h6666666666666666;
		m_pkt_tx_seq_rand.count	= 125;
		m_pkt_tx_seq_eop.data	= 64'h7777777777777777;
		m_pkt_tx_seq_eop.mod	= 3'b000;
		fork	// Sequences start in sequence, fork block waits until all are finished
		begin
		m_pkt_tx_seq_sop.start(m_xge_mac_env.m_pkt_tx_agent.m_pkt_tx_sequencer);
		m_pkt_tx_seq.start(m_xge_mac_env.m_pkt_tx_agent.m_pkt_tx_sequencer);
		m_pkt_tx_seq_rand.start(m_xge_mac_env.m_pkt_tx_agent.m_pkt_tx_sequencer);
		m_pkt_tx_seq_eop.start(m_xge_mac_env.m_pkt_tx_agent.m_pkt_tx_sequencer);
		m_pkt_tx_seq_rand_idle.start(m_xge_mac_env.m_pkt_tx_agent.m_pkt_tx_sequencer);
		end
		join

		// Send xgmii signals from tb to xgmii_rx pins on dut
		m_xgmii_seq.data	= 64'hAAAA_AAAA_AAAA_AAAA;
		m_xgmii_seq_rand.count	= 32;
		m_xgmii_seq_sop.start(m_xge_mac_env.m_xgmii_agent.m_xgmii_sequencer);
		m_xgmii_seq.start(m_xge_mac_env.m_xgmii_agent.m_xgmii_sequencer);
		m_xgmii_seq_rand.start(m_xge_mac_env.m_xgmii_agent.m_xgmii_sequencer);
		m_xgmii_seq_eop.start(m_xge_mac_env.m_xgmii_agent.m_xgmii_sequencer);

		//m_wb_seq_write.addr = 8'h00; m_wb_seq_write.data = 32'h1; // Assert TX Enable on Config Reg0
		//m_wb_seq_read.addr = 8'h84; // Read - Transmit Packet Count Register
		//m_wb_seq_write.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);
		//#50ns;
		////m_wb_seq_idle.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);
		//m_wb_seq_read.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);
		//#50ns;
		//m_wb_seq_read.addr = 8'h80; // Read - Transmit Octets Count Register
		//m_wb_seq_read.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);
		//#50ns;

		//m_wb_seq_idle.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);

		#100ns;
			reg_block.config_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $psprintf("Read config_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			reg_block.interrupt_pending_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $psprintf("Read interrupt_pending_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			reg_block.interrupt_status_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $psprintf("Read interrupt_status_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			//reg_block.interrupt_mask_reg.write(status, 64'hDEAD_BEEF_BA5E_BA11);
			//#50ns;
			reg_block.interrupt_mask_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $psprintf("Read interrupt_mask_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			//reg_block.interrupt_mask_reg.write(status, 32'hFFFF_FFFF);
			//#50ns;
			//reg_block.interrupt_mask_reg.read(status, tmp32);
			//reg_block.interrupt_mask_reg.mirror(status, UVM_CHECK);
			//`uvm_info(get_name(), $psprintf("Read interrupt_mask_reg: 32'h%8h", tmp32), UVM_LOW);
			//#50ns;
			reg_block.transmit_octets_count_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $sformatf("Read transmit_octets_count_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			reg_block.transmit_packet_count_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $sformatf("Read transmit_packet_count_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			reg_block.receive_octets_count_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $sformatf("Read receive_octets_count_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			reg_block.receive_packet_count_reg.read(status, tmp32, UVM_BACKDOOR);
			`uvm_info(get_name(), $sformatf("Read receive_packet_count_reg: 32'h%8h", tmp32), UVM_LOW);
			#50ns;
			//reg_block.interrupt_pending_reg.read(status, tmp32);
			//`uvm_info(get_name(), $sformatf("Read interrupt_pending_reg: 32'h%8h", tmp32), UVM_LOW);
			//#50ns;
			//m_xge_mac_reg_seq.start(m_xge_mac_env.m_wb_agent.m_wb_sequencer);
		#100ns;

		phase.phase_done.set_drain_time(this, 100_000);	// Sets drain time in picoseconds
		phase.drop_objection(.obj(this), .description(get_name()));

	endtask: main_phase

endclass: xge_mac_test

`endif
