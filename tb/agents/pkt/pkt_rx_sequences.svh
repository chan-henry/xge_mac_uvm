`ifndef PKT_RX_SEQUENCES_SVH
`define PKT_RX_SEQUENCES_SVH
/*
class pkt_rx_seq_rst extends uvm_sequence#(pkt_rx_transaction);

	`uvm_object_utils(pkt_rx_seq_rst)

	function new(string name = "pkt_rx_seq_rst");
		super.new(name);
	endfunction: new

	task body();
		pkt_rx_transaction tx = pkt_rx_transaction::type_id::create("rx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b0;
		tx.ren		= 1'b0;
		finish_item(tx);
	endtask: body

endclass: pkt_tx_seq_rst
*/
class pkt_rx_seq extends uvm_sequence#(pkt_rx_transaction);

	`uvm_object_utils(pkt_rx_seq)
	`uvm_declare_p_sequencer(pkt_rx_sequencer)

	pkt_rx_transaction tx;

	function new(string name = "pkt_rx_seq");
		super.new(name);
	endfunction: new

	task body();
		forever begin
			p_sequencer.tlm_fifo.get(tx);
			`uvm_info(get_name(), "Got pkt_rx_seq", UVM_NONE)
			start_item(tx);
			finish_item(tx);
		end
	endtask: body

endclass: pkt_rx_seq
/*
class pkt_tx_seq_rand extends uvm_sequence#(pkt_tx_transaction);

	`uvm_object_utils(pkt_tx_seq_rand)

	int unsigned count = 1;

	function new(string name = "pkt_tx_seq_rand");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		repeat(count) begin
			start_item(tx);
			assert(tx.randomize());
			tx.rst_n	= 1'b1;
			tx.sop		= 1'b0;
			tx.eop		= 1'b0;
			tx.val		= 1'b1;
			finish_item(tx);
		end
	endtask: body

endclass: pkt_tx_seq_rand

class pkt_tx_seq_rand_sop extends uvm_sequence#(pkt_tx_transaction);

	`uvm_object_utils(pkt_tx_seq_rand_sop)

	function new(string name = "pkt_tx_seq_rand_sop");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.sop		= 1'b1;
		tx.eop		= 1'b0;
		tx.val		= 1'b1;
		finish_item(tx);
	endtask: body

endclass: pkt_tx_seq_rand_sop

class pkt_tx_seq_rand_eop extends uvm_sequence#(pkt_tx_transaction);

	`uvm_object_utils(pkt_tx_seq_rand_eop)

	function new(string name = "pkt_tx_seq_rand_eop");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.sop		= 1'b0;
		tx.eop		= 1'b1;
		tx.val		= 1'b1;
		finish_item(tx);
	endtask: body

endclass: pkt_tx_seq_rand_eop

class pkt_tx_seq_idle extends uvm_sequence#(pkt_tx_transaction);

	bit	[63:0]	data;
	bit	[2:0]	mod;

	`uvm_object_utils(pkt_tx_seq_idle)

	function new(string name = "pkt_tx_seq_idle");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.sop		= 1'b0;
		tx.eop		= 1'b0;
		tx.val		= 1'b0;
		tx.data		= data;
		tx.mod		= mod;
		finish_item(tx);
	endtask: body

endclass: pkt_tx_seq_idle

class pkt_tx_seq extends uvm_sequence#(pkt_tx_transaction);

	bit	[63:0]	data;
	rand int unsigned count = 1;

	`uvm_object_utils(pkt_tx_seq)

	function new(string name = "pkt_tx_seq");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		repeat(count) begin
			start_item(tx);
			assert(tx.randomize());
			tx.rst_n	= 1'b1;
			tx.sop		= 1'b0;
			tx.eop		= 1'b0;
			tx.val		= 1'b1;
			tx.data		= data;
			finish_item(tx);
		end
	endtask: body

endclass: pkt_tx_seq

class pkt_tx_seq_sop extends uvm_sequence#(pkt_tx_transaction);

	bit	[63:0]	data;

	`uvm_object_utils(pkt_tx_seq_sop)

	function new(string name = "pkt_tx_seq_sop");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.sop		= 1'b1;
		tx.eop		= 1'b0;
		tx.val		= 1'b1;
		tx.data		= data;
		finish_item(tx);
	endtask: body

endclass: pkt_tx_seq_sop

class pkt_tx_seq_eop extends uvm_sequence#(pkt_tx_transaction);

	bit	[63:0]	data;
	bit	[2:0]	mod	= 3'b000;

	`uvm_object_utils(pkt_tx_seq_eop)

	function new(string name = "pkt_tx_seq_eop");
		super.new(name);
	endfunction: new

	task body();
		pkt_tx_transaction tx = pkt_tx_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.sop		= 1'b0;
		tx.eop		= 1'b1;
		tx.val		= 1'b1;
		tx.data		= data;
		tx.mod		= mod;
		finish_item(tx);
	endtask: body

endclass: pkt_tx_seq_eop

// TODO: pkt_tx_seq_sop_eop	- Single frame packet
*/
`endif
