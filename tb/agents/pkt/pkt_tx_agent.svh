`ifndef PKT_TX_AGENT_SVH
`ifndef PKT_TX_AGENT_SVH

class pkt_tx_agent extends uvm_agent;

	`uvm_component_utils(pkt_tx_agent)

	pkt_tx_driver				m_pkt_tx_driver;
	uvm_sequencer#(pkt_tx_transaction)	m_pkt_tx_sequencer;

	function new(string name = "pkt_tx_agent", uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		m_pkt_tx_driver		= pkt_tx_driver::type_id::create("m_pkt_tx_driver", this);
		m_pkt_tx_sequencer	= uvm_sequencer#(pkt_tx_transaction)::type_id::create("m_pkt_tx_sequencer", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		m_pkt_tx_driver.seq_item_port.connect(m_pkt_tx_sequencer.seq_item_export);
	endfunction: connect_phase

	task pre_reset_phase(uvm_phase phase);
		m_pkt_tx_sequencer.stop_sequences();
		->m_pkt_tx_driver.reset_driver;
	endtask: pre_reset_phase
endclass: pkt_tx_agent

`endif
