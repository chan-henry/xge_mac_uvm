`ifndef PKT_RX_AGENT_SVH
`ifndef PKT_RX_AGENT_SVH

class pkt_rx_agent extends uvm_agent;

	`uvm_component_utils(pkt_rx_agent)

	pkt_rx_monitor		m_pkt_rx_monitor;
	pkt_rx_driver		m_pkt_rx_driver;
	pkt_rx_sequencer	m_pkt_rx_sequencer;

	function new(string name = "pkt_rx_agent", uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		m_pkt_rx_monitor	= pkt_rx_monitor::type_id::create("m_pkt_rx_monitor", this);
		m_pkt_rx_driver		= pkt_rx_driver::type_id::create("m_pkt_rx_driver", this);
		m_pkt_rx_sequencer	= pkt_rx_sequencer::type_id::create("m_pkt_rx_sequencer", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		m_pkt_rx_monitor.ap.connect(m_pkt_rx_sequencer.exprt);
		m_pkt_rx_driver.seq_item_port.connect(m_pkt_rx_sequencer.seq_item_export);
	endfunction: connect_phase

endclass: pkt_rx_agent

`endif
