`ifndef PKT_RX_MONITOR_SVH
`define PKT_RX_MONITOR_SVH

class pkt_rx_monitor extends uvm_monitor;

	`uvm_component_utils(pkt_rx_monitor)

	uvm_analysis_port#(pkt_rx_transaction) ap;
	virtual pkt_rx pkt_rx_if;

	function new(string name = "pkt_rx_monitor", uvm_component parent);
		super.new(name, parent);
		ap = new(.name("ap"), .parent(this));
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual pkt_rx)::get(.cntxt(this), .inst_name(""), .field_name("pkt_rx_vif"), .value(pkt_rx_if))) begin
			`uvm_fatal(get_name(), "Failed to get virtual interface for pkt_rx_monitor")
		end
	endfunction: build_phase

	task run_phase(uvm_phase phase);
		pkt_rx_transaction tx;

		forever begin
			if(1'b0 == pkt_rx_if.pkt_rx_rst_n) begin
				@(posedge pkt_rx_if.pkt_rx_rst_n);
			end

			@(pkt_rx_if.cb);
			if(1'b1 == pkt_rx_if.pkt_rx_avail) begin
				tx = pkt_rx_transaction::type_id::create("tx");
				phase.raise_objection(.obj(this), .description(get_name()));
				tx.ren = 1'b1;
				ap.write(tx);
				`uvm_info(get_name(), "PKT_RX_MON wrote REN = 1'b1 out to PKT_RX_SEQUENCER", UVM_NONE)
				phase.drop_objection(.obj(this), .description(get_name()));
				@(negedge pkt_rx_if.pkt_rx_avail);
			end

			if(1'b1 == pkt_rx_if.pkt_rx_eop) begin
				tx = pkt_rx_transaction::type_id::create("tx");
				phase.raise_objection(.obj(this), .description(get_name()));
				tx.ren = 1'b0;
				ap.write(tx);
				`uvm_info(get_name(), "PKT_RX_MON wrote REN = 1'b0 out to PKT_RX_SEQUENCER", UVM_NONE)
				phase.drop_objection(.obj(this), .description(get_name()));
			end
			
		end
	endtask: run_phase
endclass: pkt_rx_monitor

`endif //PKT_RX_MONITOR_SVH
