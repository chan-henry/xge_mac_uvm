`ifndef PKT_TX_TRANSACTION_SVH
`define PKT_TX_TRANSACTION_SVH

class pkt_tx_transaction extends uvm_sequence_item;

	`uvm_object_utils(pkt_tx_transaction)

	rand bit [63:0]	data;
	rand bit	val;
	rand bit	sop;
	rand bit	eop;
	rand bit [2:0]	mod;
	rand bit	full;

	function new(string name = "pkt_tx_transaction");
		super.new(name);
	endfunction: new

endclass: pkt_tx_transaction

`endif
