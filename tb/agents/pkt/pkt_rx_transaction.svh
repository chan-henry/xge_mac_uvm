`ifndef PKT_RX_TRANSACTION_SVH
`define PKT_RX_TRANSACTION_SVH

class pkt_rx_transaction extends uvm_sequence_item;

	`uvm_object_utils(pkt_rx_transaction)

	bit [63:0]	data;
	bit		val;
	bit		sop;
	bit		eop;
	bit [2:0]	mod;
	bit		avail;
	rand bit	ren;

	function new(string name = "pkt_rx_transaction");
		super.new(name);
	endfunction: new

endclass: pkt_rx_transaction

`endif
