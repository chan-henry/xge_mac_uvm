`ifndef PKT_RX_DRIVER_SVH
`define PKT_RX_DRIVER_SVH

class pkt_rx_driver extends uvm_driver#(pkt_rx_transaction);

	`uvm_component_utils(pkt_rx_driver)

	virtual pkt_rx	pkt_rx_if;

	function new(string name = "pkt_rx_driver", uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual pkt_rx)::get(.cntxt(this), .inst_name(""), .field_name("pkt_rx_vif"), .value(pkt_rx_if))) begin
			`uvm_fatal(get_name(), "Failed to get virtual interface for pkt_rx_driver")
		end
	endfunction: build_phase

	task reset_phase(uvm_phase phase);
		phase.raise_objection(this);
		pkt_rx_if.master.pkt_rx_ren <= 1'b0;
		phase.drop_objection(this);
	endtask: reset_phase

	task run_phase(uvm_phase phase);
		pkt_rx_transaction tx;

		forever begin

			seq_item_port.get_next_item(tx);
			phase.raise_objection(.obj(this), .description(get_name()));

			@(pkt_rx_if.cb);
			`uvm_info(get_name(), "Driving pkt_rx_ren", UVM_LOW)
			pkt_rx_if.cb.pkt_rx_ren	<= tx.ren;

			seq_item_port.item_done();

			phase.drop_objection(.obj(this), .description(get_name()));
		end
	endtask: run_phase

endclass: pkt_rx_driver

`endif
