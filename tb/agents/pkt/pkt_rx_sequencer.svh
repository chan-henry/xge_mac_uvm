`ifndef PKT_RX_SEQUENCER_SVH
`define PKT_RX_SEQUENCER_SVH
class pkt_rx_sequencer extends uvm_sequencer#(pkt_rx_transaction);

	`uvm_component_utils(pkt_rx_sequencer)

	uvm_analysis_export#(pkt_rx_transaction) exprt;
	uvm_tlm_analysis_fifo#(pkt_rx_transaction) tlm_fifo;

	function new(string name = "pkt_rx_sequencer", uvm_component parent);
		super.new(name, parent);

		exprt = new("exprt", this);
		tlm_fifo = new("tlm_fifo", this);
	endfunction: new

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		exprt.connect(tlm_fifo.analysis_export);
	endfunction: connect_phase
endclass: pkt_rx_sequencer
`endif
