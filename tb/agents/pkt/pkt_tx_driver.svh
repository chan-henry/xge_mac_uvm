`ifndef PKT_TX_DRIVER_SVH
`define PKT_TX_DRIVER_SVH

class pkt_tx_driver extends uvm_driver#(pkt_tx_transaction);

	`uvm_component_utils(pkt_tx_driver)

	event reset_driver;

	virtual pkt_tx		pkt_tx_if;
	pkt_tx_transaction	tx;

	function new(string name = "pkt_tx_driver", uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual pkt_tx)::get(.cntxt(this), .inst_name(""), .field_name("pkt_tx_vif"), .value(pkt_tx_if))) begin
			`uvm_fatal(get_name(), "Failed to get virtual interface for pkt_tx_driver")
		end
	endfunction: build_phase

	//task pre_reset_phase(uvm_phase phase);
	//endtask: pre_reset_phase

	task reset_phase(uvm_phase phase);
		phase.raise_objection(this);
		pkt_tx_if.master.pkt_tx_data	<= 'h0;
		pkt_tx_if.master.pkt_tx_val	<= 'h0;
		pkt_tx_if.master.pkt_tx_sop	<= 'h0;
		pkt_tx_if.master.pkt_tx_eop	<= 'h0;
		pkt_tx_if.master.pkt_tx_mod	<= 'h0;
		phase.drop_objection(this);
	endtask: reset_phase

	task drive_stimulus;
		forever begin
			seq_item_port.get_next_item(tx);
			@(pkt_tx_if.cb);
			//`uvm_info(get_name(), "Entered drive_stimulus", UVM_NONE)
			pkt_tx_if.cb.pkt_tx_data	<= tx.data;
			pkt_tx_if.cb.pkt_tx_val		<= tx.val;
			pkt_tx_if.cb.pkt_tx_sop		<= tx.sop;
			pkt_tx_if.cb.pkt_tx_eop		<= tx.eop;
			pkt_tx_if.cb.pkt_tx_mod		<= tx.mod;
			seq_item_port.item_done();
		end
	endtask: drive_stimulus

	task run_phase(uvm_phase phase);

		forever begin
			fork
				drive_stimulus();
			join_none

			@(reset_driver);
			disable fork;
		end
	endtask: run_phase

endclass: pkt_tx_driver

`endif
