`ifndef WB_DRIVER_SVH
`define WB_DRIVER_SVH

class wb_driver extends uvm_driver#(wb_transaction);

	`uvm_component_utils(wb_driver)

	virtual wishbone	wb_if;
	wb_transaction		tx;
	event			reset_driver;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual wishbone)::get(.cntxt(this), .inst_name(""), .field_name("wb_vif"), .value(wb_if))) begin
			`uvm_fatal(get_name(), "Failed to get virtual interface for wb_driver")
		end
	endfunction: build_phase

	task reset_phase(uvm_phase phase);
		phase.raise_objection(this);
		wb_if.master.dat_m2s	<= 'h0;
		wb_if.master.adr	<= 'h0;
		wb_if.master.we		<= 'h0;
		wb_if.master.stb	<= 'h0;
		wb_if.master.cyc	<= 'h0;
		phase.drop_objection(this);
	endtask: reset_phase

	task drive_stimulus;
		forever begin
			seq_item_port.get_next_item(tx);

			@wb_if.master_cb;
				wb_if.master_cb.dat_m2s	<= tx.wb_dat_m2s;
				wb_if.master_cb.adr	<= tx.wb_adr;
				wb_if.master_cb.we	<= tx.wb_we;
				wb_if.master_cb.stb	<= tx.wb_stb;
				wb_if.master_cb.cyc	<= tx.wb_cyc;
			@wb_if.master_cb;
				// This should be a while loop (wait multiple cycles), but using 'while' will loop forever and not end the test
				while(!wb_if.master.ack) begin	// If no acknowledgment yet, then keep waiting
					@wb_if.master_cb;
				end
			@wb_if.master_cb;
				wb_if.master_cb.we	<= 1'b0;
				wb_if.master_cb.stb	<= 1'b0;
				wb_if.master_cb.cyc	<= 1'b0;

			seq_item_port.item_done();
		end
	endtask: drive_stimulus

	task run_phase(uvm_phase phase);
		forever begin
			fork
				drive_stimulus();
			join_none

			@(reset_driver);
			disable fork;
		end
	endtask: run_phase

endclass: wb_driver

`endif
