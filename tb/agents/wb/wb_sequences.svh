`ifndef WB_SEQUENCES_SVH
`define WB_SEQUENCES_SVH

class wb_sequence extends uvm_sequence#(wb_transaction);

	`uvm_object_utils(wb_sequence)

	function new(string name = "wb_sequence");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		finish_item(tx);
	endtask: body

endclass: wb_sequence

class wb_seq_reset extends uvm_sequence#(wb_transaction);

	`uvm_object_utils(wb_seq_reset)

	function new(string name = "wb_seq_reset");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b1;
		// TODO: Zero out the wb signals
		finish_item(tx);
	endtask: body

endclass: wb_seq_reset

class wb_seq_rand_idle extends uvm_sequence#(wb_transaction);

	`uvm_object_utils(wb_seq_rand_idle)

	function new(string name = "wb_seq_rand_idle");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b0;
		tx.wb_stb	= 1'b0;
		tx.wb_cyc	= 1'b0;
		finish_item(tx);
	endtask: body

endclass: wb_seq_rand_idle

class wb_seq_rand_write extends uvm_sequence#(wb_transaction);

	`uvm_object_utils(wb_seq_rand_write)

	function new(string name = "wb_seq_rand_write");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b0;
		tx.wb_we	= 1'b1;
		tx.wb_stb	= 1'b1;
		tx.wb_cyc	= 1'b1;
		finish_item(tx);
	endtask: body

endclass: wb_seq_rand_write

class wb_seq_rand_read extends uvm_sequence#(wb_transaction);

	`uvm_object_utils(wb_seq_rand_read)

	function new(string name = "wb_seq_rand_read");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b0;
		tx.wb_we	= 1'b0;
		tx.wb_stb	= 1'b1;
		tx.wb_cyc	= 1'b1;
		finish_item(tx);
	endtask: body

endclass: wb_seq_rand_read

class wb_seq_idle extends uvm_sequence#(wb_transaction);

	`uvm_object_utils(wb_seq_idle)

	function new(string name = "wb_seq_idle");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b0;
		tx.wb_stb	= 1'b0;
		tx.wb_cyc	= 1'b0;
		finish_item(tx);
	endtask: body

endclass: wb_seq_idle

class wb_seq_write extends uvm_sequence#(wb_transaction);

	`uvm_object_param_utils(wb_seq_write)

	bit	[7:0]	addr = 8'hBB;			// Default addr
	bit	[63:0]	data = 64'hb0a050e0b0a01010;	// Default data

	function new(string name = "wb_seq_write");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b0;
		tx.wb_we	= 1'b1;
		tx.wb_stb	= 1'b1;
		tx.wb_cyc	= 1'b1;
		tx.wb_adr	= addr;
		tx.wb_dat_m2s	= data;
		finish_item(tx);
	endtask: body

endclass: wb_seq_write

class wb_seq_read extends uvm_sequence#(wb_transaction);

	`uvm_object_param_utils(wb_seq_read)

	bit	[7:0]	addr = 8'hBB;	// Default addr

	function new(string name = "wb_seq_read");
		super.new(name);
	endfunction: new

	task body();
		wb_transaction tx = wb_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.wb_rst	= 1'b0;
		tx.wb_we	= 1'b0;
		tx.wb_stb	= 1'b1;
		tx.wb_cyc	= 1'b1;
		tx.wb_adr	= addr;
		finish_item(tx);
	endtask: body

endclass: wb_seq_read

class wb_reg_seq_write extends uvm_reg_sequence;

	`uvm_object_utils(wb_reg_seq_write)

	function new(string name = "wb_reg_seq_write");
		super.new(name);
	endfunction: new

	virtual task body();
		xge_mac_reg_block	reg_block;
		bit	[7:0]	addr;
		bit	[63:0]	data;
		uvm_status_e	status;
		uvm_reg_data_t	value;

		$cast( reg_block, model);

		write_reg( reg_block.interrupt_mask_reg, status, data[31:0]);
		read_reg( reg_block.interrupt_mask_reg, status, value);
	endtask: body

endclass: wb_reg_seq_write

`endif
