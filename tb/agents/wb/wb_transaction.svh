`ifndef WB_TRANSACTION_SVH
`define WB_TRANSACTION_SVH

class wb_transaction extends uvm_sequence_item;

	`uvm_object_utils(wb_transaction)

	rand bit	wb_rst;
	rand bit [7:0]	wb_adr;		// Master to Slave
	rand bit	wb_cyc;		// Master to Slave
	rand bit [31:0]	wb_dat_s2m;	// Slave to Master
	rand bit	wb_stb;		// Master to Slave
	rand bit	wb_we;		// Master to Slave
	rand bit	wb_sel;		// Master to Slave
	rand bit	wb_ack;		// Slave to Master
	rand bit [31:0]	wb_dat_m2s;	// Master to Slave
	rand bit	wb_int;		// Slave to Master

	function new(string name="wb_transaction");
		super.new(name);
	endfunction: new

	function string convert2string();
		//string s = super.convert2string();

		// TODO: Implement this? or leave it out for less clutter?
	endfunction: convert2string

endclass: wb_transaction

`endif
