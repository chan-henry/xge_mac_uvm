`ifndef WB_AGENT_SVH
`define WB_AGENT_SVH

class wb_agent extends uvm_agent;

	`uvm_component_utils(wb_agent)

	wb_driver			m_wb_driver;
	uvm_sequencer#(wb_transaction)	m_wb_sequencer;
	xge_mac_reg_adapter		reg_adapter;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		m_wb_driver	= wb_driver::type_id::create("m_wb_driver", this);
		m_wb_sequencer	= uvm_sequencer#(wb_transaction)::type_id::create("m_wb_sequencer", this);
		reg_adapter	= xge_mac_reg_adapter::type_id::create("reg_adapter");
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		m_wb_driver.seq_item_port.connect(m_wb_sequencer.seq_item_export);
	endfunction: connect_phase

	task pre_reset_phase(uvm_phase phase);
		m_wb_sequencer.stop_sequences();
		->m_wb_driver.reset_driver;
	endtask: pre_reset_phase

endclass: wb_agent

`endif
