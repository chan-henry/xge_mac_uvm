`ifndef RST_DRIVER_SVH
`define RST_DRIVER_SVH
class rst_driver extends uvm_driver;

	`uvm_component_utils(rst_driver)

	virtual	rst_if	m_rst_if;
	// TODO: Make these assignments via config objects?
	bit	sync_assert = 0;	// Synchronous or Asynchronous reset assertion
	bit	sync_deassert = 1;	// Synchronous or Asynchronous reset deassertion

	function new(string name = "rst_driver", uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		if(!uvm_config_db#(virtual rst_if)::get(.cntxt(this), .inst_name(""), .field_name("rst_if"), .value(m_rst_if))) begin
			`uvm_fatal(get_name(), "Failed to get rst_if for rst_driver")
		end
	endfunction: build_phase

	task reset_phase(uvm_phase phase);
		phase.raise_objection(this);

			if(1'b1 == sync_assert) begin
				@(posedge m_rst_if.clk);
			end

			m_rst_if.rst	<= 1'b1;
			m_rst_if.rst_n	<= 1'b0;

			#20ns;		// TODO: Make this parameterizable via config or something

			if(1'b1 == sync_deassert) begin
				@(posedge m_rst_if.clk);
			end

			m_rst_if.rst	<= 1'b0;
			m_rst_if.rst_n	<= 1'b1;

		phase.drop_objection(this);
	endtask: reset_phase

endclass: rst_driver
`endif //XGE_MAC_DRIVER_SVH
