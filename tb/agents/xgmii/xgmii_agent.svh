`ifndef XGMII_AGENT_SVH
`define XGMII_AGENT_SVH

class xgmii_agent extends uvm_agent;

	`uvm_component_utils(xgmii_agent)

	xgmii_slave_driver			m_xgmii_slave_driver;
	uvm_sequencer#(xgmii_transaction)	m_xgmii_sequencer;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		m_xgmii_slave_driver	= xgmii_slave_driver::type_id::create("m_xgmii_slave_driver", this);
		m_xgmii_sequencer	= uvm_sequencer#(xgmii_transaction)::type_id::create("m_xgmii_sequencer", this);
	endfunction: build_phase

	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);

		m_xgmii_slave_driver.seq_item_port.connect(m_xgmii_sequencer.seq_item_export);
	endfunction: connect_phase

endclass: xgmii_agent

`endif
