`ifndef XGMII_SEQUENCES_SVH
`define XGMII_SEQUENCES_SVH

class xgmii_seq extends uvm_sequence#(xgmii_transaction);

	`uvm_object_utils(xgmii_seq)

	bit [63:0]	data = 64'hFFFF_FFFF_FFFF_FFFF;

	function new(string name = "xgmii_seq");
		super.new(name);
	endfunction: new

	task body();
		xgmii_transaction tx = xgmii_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.ctrl		= 8'h00;
		tx.data		= data;
		finish_item(tx);
	endtask: body

endclass: xgmii_seq

class xgmii_seq_reset extends uvm_sequence#(xgmii_transaction);

	`uvm_object_utils(xgmii_seq_reset)

	function new(string name = "xgmii_seq_reset");
		super.new(name);
	endfunction: new

	task body();
		xgmii_transaction tx = xgmii_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b0;
		finish_item(tx);
	endtask: body

endclass: xgmii_seq_reset

class xgmii_seq_rand extends uvm_sequence#(xgmii_transaction);

	`uvm_object_utils(xgmii_seq_rand)
	
	int unsigned count = 1;

	constraint max_frame_data_count { count inside {[1:126]}; }

	function new(string name = "xgmii_seq_rand");
		super.new(name);
	endfunction: new

	task body();
		xgmii_transaction tx = xgmii_transaction::type_id::create("tx", .contxt(get_full_name()));
		repeat(count) begin
			start_item(tx);
			assert(tx.randomize());
			tx.rst_n	= 1'b1;
			tx.ctrl		= 8'h00;
			finish_item(tx);
		end
	endtask: body

endclass: xgmii_seq_rand

class xgmii_seq_sop extends uvm_sequence#(xgmii_transaction);

	`uvm_object_utils(xgmii_seq_sop)

	function new(string name = "xgmii_seq_sop");
		super.new(name);
	endfunction: new

	task body();
		xgmii_transaction tx = xgmii_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.ctrl		= 8'h0000_0001;
		tx.data		= 64'hD5_5555_5555_5555_FB;
		finish_item(tx);
	endtask: body

endclass: xgmii_seq_sop

class xgmii_seq_eop extends uvm_sequence#(xgmii_transaction);

	`uvm_object_utils(xgmii_seq_eop)

	function new(string name = "xgmii_seq_eop");
		super.new(name);
	endfunction: new

	task body();
		xgmii_transaction tx = xgmii_transaction::type_id::create("tx", .contxt(get_full_name()));
		start_item(tx);
		assert(tx.randomize());
		tx.rst_n	= 1'b1;
		tx.ctrl		= 8'hFF;
		tx.data		= 64'h0707_0707_0707_07FD;
		finish_item(tx);
	endtask: body

endclass: xgmii_seq_eop

`endif
