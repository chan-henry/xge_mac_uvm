`ifndef XGMII_TRANSACTION_SVH
`define XGMII_TRANSACTION_SVH

class xgmii_transaction extends uvm_sequence_item;

	`uvm_object_utils(xgmii_transaction)

	rand bit	rst_n;	// Reset
	rand bit [7:0]	ctrl;	// Control
	rand bit [63:0]	data;	// Data

	function new(string name = "xgmii_transaction");
		super.new(name);
	endfunction: new

endclass: xgmii_transaction

`endif
