`ifndef XGMII_DRIVER_SVH
`define XGMII_DRIVER_SVH

class xgmii_slave_driver extends uvm_driver#(xgmii_transaction);

	`uvm_component_utils(xgmii_slave_driver)

	virtual xgmii	xgmii_if;

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

		if(!uvm_config_db#(virtual xgmii)::get(.cntxt(this), .inst_name(""), .field_name("xgmii_vif"), .value(xgmii_if))) begin
			`uvm_fatal(get_name(), "Failed to get virtual interface for xgmii_tx_driver")
		end
	endfunction: build_phase

	task reset_phase(uvm_phase phase);
		xgmii_if.rxc	<= 'hFFFF;
		xgmii_if.rxd	<= 'h0707_0707_0707_0707;
	endtask: reset_phase

	task run_phase(uvm_phase phase);
		xgmii_transaction tx;

		forever begin
			seq_item_port.get_next_item(tx);
			phase.raise_objection(.obj(this), .description(get_name()));

			@xgmii_if.cb;
				xgmii_if.rxc		= tx.ctrl;
				xgmii_if.rxd		= tx.data;

			seq_item_port.item_done();
			phase.drop_objection(.obj(this), .description(get_name()));
		end
	endtask: run_phase

endclass: xgmii_slave_driver

`endif
