`ifndef RAL_XGE_MAC
`define RAL_XGE_MAC

// FIXME: Most of the count registers are cleared on read
//		even though they it is not specified as such
//		in the documentation. They are Read-only with
//		clear on reset in the documentation.

class Config_Reg extends uvm_reg;

	`uvm_object_utils(Config_Reg)

	rand uvm_reg_field reserved;
	rand uvm_reg_field tx_enable;

	function new(string name = "Config_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		reserved = uvm_reg_field::type_id::create("reserved");
		reserved.configure(	.parent		(this),
					.size		(31),
					.lsb_pos	(1),
					.access		("RW"),
					.volatile	(0),
					.reset		(31'h0),
					.has_reset	(1),
					.is_rand	(0),
					.individually_accessible(0)
					);
		tx_enable = uvm_reg_field::type_id::create("tx_enable");
		tx_enable.configure(	.parent		(this),
					.size		(1),
					.lsb_pos	(0),
					.access		("RW"),
					.volatile	(0),
					.reset		(1'b1),
					.has_reset	(1),
					.is_rand	(1),
					.individually_accessible(0)
					);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_config0"), .offset(0), .size(1));

	endfunction: build


endclass: Config_Reg

class Interrupt_Pending_Reg extends uvm_reg;

	`uvm_object_utils(Interrupt_Pending_Reg)

	rand uvm_reg_field reserved;
	rand uvm_reg_field rx_fragment_error;
	rand uvm_reg_field rx_crc_error;
	rand uvm_reg_field rx_pause_frame;
	rand uvm_reg_field remote_fault;
	rand uvm_reg_field local_fault;
	rand uvm_reg_field rx_data_fifo_underflow;
	rand uvm_reg_field rx_data_fifo_overflow;
	rand uvm_reg_field tx_data_fifo_underflow;
	rand uvm_reg_field tx_data_fifo_overflow;

	function new(string name="Interrupt_Pending_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		// TODO: There is a status_lenght_error (note the typo) in the DUT
		//		but not in the documentation. It maps to
		//		bit position 9 in this reg, reducing the reserved
		//		bits to 22.
		reserved = uvm_reg_field::type_id::create("reserved");
		reserved.configure(	.parent		(this),
					.size		(23),
					.lsb_pos	(9),
					.access		("RC"),
					.volatile	(0),
					.reset		(23'h0),
					.has_reset	(1),
					.is_rand	(1),
					.individually_accessible(0)
					);
		rx_fragment_error = uvm_reg_field::type_id::create("rx_fragment_error");
		rx_fragment_error.configure(	.parent		(this),
						.size		(1),
						.lsb_pos	(8),
						.access		("RC"),
						.volatile	(1),
						.reset		(1'b0),
						.has_reset	(1),
						.is_rand	(1),
						.individually_accessible(0)
						);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[8]"), .offset(8), .size(1));
		rx_crc_error = uvm_reg_field::type_id::create("rx_crc_error");
		rx_crc_error.configure(	.parent		(this),
					.size		(1),
					.lsb_pos	(7),
					.access		("RC"),
					.volatile	(1),
					.reset		(1'b0),
					.has_reset	(1),
					.is_rand	(1),
					.individually_accessible(0)
					);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[7]"), .offset(7), .size(1));
		rx_pause_frame = uvm_reg_field::type_id::create("rx_pause_frame");
		rx_pause_frame.configure(	.parent		(this),
						.size		(1),
						.lsb_pos	(6),
						.access		("RC"),
						.volatile	(1),
						.reset		(1'b0),
						.has_reset	(1),
						.is_rand	(1),
						.individually_accessible(0)
						);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[6]"), .offset(6), .size(1));
		remote_fault = uvm_reg_field::type_id::create("remote_fault");
		remote_fault.configure(	.parent		(this),
					.size		(1),
					.lsb_pos	(5),
					.access		("RC"),
					.volatile	(1),
					.reset		(1'b0),
					.has_reset	(1),
					.is_rand	(1),
					.individually_accessible(0)
					);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[5]"), .offset(5), .size(1));
		local_fault = uvm_reg_field::type_id::create("local_fault");
		local_fault.configure(	.parent		(this),
					.size		(1),
					.lsb_pos	(4),
					.access		("RC"),
					.volatile	(1),
					.reset		(1'b0),
					.has_reset	(1),
					.is_rand	(1),
					.individually_accessible(0)
					);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[4]"), .offset(4), .size(1));
		rx_data_fifo_underflow = uvm_reg_field::type_id::create("rx_data_fifo_underflow");
		rx_data_fifo_underflow.configure(	.parent		(this),
							.size		(1),
							.lsb_pos	(3),
							.access		("RC"),
							.volatile	(1),
							.reset		(1'b0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[3]"), .offset(3), .size(1));
		rx_data_fifo_overflow = uvm_reg_field::type_id::create("rx_data_fifo_overflow");
		rx_data_fifo_overflow.configure(	.parent		(this),
							.size		(1),
							.lsb_pos	(2),
							.access		("RC"),
							.volatile	(1),
							.reset		(1'b0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[2]"), .offset(2), .size(1));
		tx_data_fifo_underflow = uvm_reg_field::type_id::create("tx_data_fifo_underflow");
		tx_data_fifo_underflow.configure(	.parent		(this),
							.size		(1),
							.lsb_pos	(1),
							.access		("RC"),
							.volatile	(1),
							.reset		(1'b0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[1]"), .offset(1), .size(1));
		tx_data_fifo_overflow = uvm_reg_field::type_id::create("tx_data_fifo_overflow");
		tx_data_fifo_overflow.configure(	.parent		(this),
							.size		(1),
							.lsb_pos	(0),
							.access		("RC"),
							.volatile	(1),
							.reset		(1'b0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_pending[0]"), .offset(0), .size(1));
	endfunction: build

endclass: Interrupt_Pending_Reg

class Interrupt_Status_Reg extends uvm_reg;

	`uvm_object_utils(Interrupt_Status_Reg)

	rand uvm_reg_field interrupt_status;

	function new(string name="Interrupt_Status_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		interrupt_status = uvm_reg_field::type_id::create("interrupt_status");
		interrupt_status.configure(	.parent		(this),
						.size		(32),
						.lsb_pos	(0),
						.access		("RO"),
						.volatile	(1),
						.reset		(32'h0),
						.has_reset	(1),
						.is_rand	(1),
						.individually_accessible(0)
						);
		// Two sets of signals, either should work (Note: the missing length error bit #9)

		//add_hdl_path_slice(.name("sync_clk_wb0.status_fragment_error"), .offset(8), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_crc_error"), .offset(7), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_pause_frame_rx"), .offset(6), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_remote_fault"), .offset(5), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_local_fault"), .offset(4), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_rxdfifo_udflow"), .offset(3), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_rxdfifo_ovflow"), .offset(2), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_txdfifo_udflow"), .offset(1), .size(1));
		//add_hdl_path_slice(.name("sync_clk_wb0.status_txdfifo_ovflow"), .offset(0), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[8]"), .offset(8), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[7]"), .offset(7), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[6]"), .offset(6), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[5]"), .offset(5), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[4]"), .offset(4), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[3]"), .offset(3), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[2]"), .offset(2), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[1]"), .offset(1), .size(1));
		add_hdl_path_slice(.name("wishbone_if0.int_sources[0]"), .offset(0), .size(1));
	endfunction: build

endclass: Interrupt_Status_Reg

class Interrupt_Mask_Reg extends uvm_reg;

	`uvm_object_utils(Interrupt_Mask_Reg)

	rand uvm_reg_field interrupt_status;

	function new(string name="Interrupt_Mask_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		interrupt_status = uvm_reg_field::type_id::create("interrupt_status");
		interrupt_status.configure(	.parent		(this),
						.size		(32),
						.lsb_pos	(0),
						.access		("RW"),
						.volatile	(0),
						.reset		(32'h0),
						.has_reset	(1),
						.is_rand	(1),
						.individually_accessible(0)
						);
		// Note the missing length error bit #9
		add_hdl_path_slice(.name("wishbone_if0.cpureg_int_mask[8:0]"), .offset(0), .size(32));
	endfunction: build

endclass: Interrupt_Mask_Reg

class Transmit_Octets_Count_Reg extends uvm_reg;

	`uvm_object_utils(Transmit_Octets_Count_Reg)

	rand uvm_reg_field transmit_octets_count;

	function new(string name="Transmit_Octets_Count_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		transmit_octets_count = uvm_reg_field::type_id::create("transmit_octets_count");
		transmit_octets_count.configure(	.parent		(this),
							.size		(32),
							.lsb_pos	(0),
							.access		("RO"),
							.volatile	(1),
							.reset		(32'h0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("stats0.stats_sm0.stats_tx_octets"), .offset(0), .size(32));
	endfunction: build

endclass: Transmit_Octets_Count_Reg

class Transmit_Packet_Count_Reg extends uvm_reg;

	`uvm_object_utils(Transmit_Packet_Count_Reg)

	rand uvm_reg_field transmit_packet_count;

	function new(string name="Transmit_Packet_Count_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		transmit_packet_count = uvm_reg_field::type_id::create("transmit_packet_count");
		transmit_packet_count.configure(	.parent		(this),
							.size		(32),
							.lsb_pos	(0),
							.access		("RO"),
							.volatile	(1),
							.reset		(32'h0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("stats0.stats_sm0.stats_tx_pkts"), .offset(0), .size(32));
	endfunction: build

endclass: Transmit_Packet_Count_Reg

class Receive_Octets_Count_Reg extends uvm_reg;

	`uvm_object_utils(Receive_Octets_Count_Reg)

	rand uvm_reg_field receive_octets_count;

	function new(string name="receive_octet_count_reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		receive_octets_count = uvm_reg_field::type_id::create("receive_octets_count");
		receive_octets_count.configure(	.parent		(this),
							.size		(32),
							.lsb_pos	(0),
							.access		("RO"),
							.volatile	(1),
							.reset		(32'h0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("stats0.stats_sm0.stats_rx_octets"), .offset(0), .size(32));
	endfunction: build

endclass: Receive_Octets_Count_Reg

class Receive_Packet_Count_Reg extends uvm_reg;

	`uvm_object_utils(Receive_Packet_Count_Reg)

	rand uvm_reg_field receive_packet_count;

	function new(string name="Receive_Packet_Count_Reg");
		super.new(.name(name), .n_bits(32), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		receive_packet_count = uvm_reg_field::type_id::create("receive_packet_count");
		receive_packet_count.configure(	.parent		(this),
							.size		(32),
							.lsb_pos	(0),
							.access		("RO"),
							.volatile	(1),
							.reset		(32'h0),
							.has_reset	(1),
							.is_rand	(1),
							.individually_accessible(0)
							);
		add_hdl_path_slice(.name("stats0.stats_sm0.stats_rx_pkts"), .offset(0), .size(32));
	endfunction: build

endclass: Receive_Packet_Count_Reg

class xge_mac_reg_block extends uvm_reg_block;

	`uvm_object_utils(xge_mac_reg_block)

	rand Config_Reg			config_reg;
	rand Interrupt_Pending_Reg	interrupt_pending_reg;
	rand Interrupt_Status_Reg	interrupt_status_reg;
	rand Interrupt_Mask_Reg		interrupt_mask_reg;
	rand Transmit_Octets_Count_Reg	transmit_octets_count_reg;
	rand Transmit_Packet_Count_Reg	transmit_packet_count_reg;
	rand Receive_Octets_Count_Reg	receive_octets_count_reg;
	rand Receive_Packet_Count_Reg	receive_packet_count_reg;
	uvm_reg_map			reg_map;

	function new(string name = "xge_mac_reg_block");
		super.new(.name(name), .has_coverage(UVM_NO_COVERAGE));
	endfunction: new

	function void build();
		config_reg = Config_Reg::type_id::create("config_reg");
		config_reg.configure(.blk_parent(this));
		config_reg.build();
		interrupt_pending_reg = Interrupt_Pending_Reg::type_id::create("interrupt_pending_reg");
		interrupt_pending_reg.configure(.blk_parent(this));
		interrupt_pending_reg.build();
		interrupt_status_reg = Interrupt_Status_Reg::type_id::create("interrupt_status_reg");
		interrupt_status_reg.configure(.blk_parent(this));
		interrupt_status_reg.build();
		interrupt_mask_reg = Interrupt_Mask_Reg::type_id::create("interrupt_mask_reg");
		interrupt_mask_reg.configure(.blk_parent(this));
		interrupt_mask_reg.build();
		transmit_octets_count_reg = Transmit_Octets_Count_Reg::type_id::create("transmit_octets_count_reg");
		transmit_octets_count_reg.configure(.blk_parent(this));
		transmit_octets_count_reg.build();
		transmit_packet_count_reg = Transmit_Packet_Count_Reg::type_id::create("transmit_packet_count_reg");
		transmit_packet_count_reg.configure(.blk_parent(this));
		transmit_packet_count_reg.build();
		receive_octets_count_reg = Receive_Octets_Count_Reg::type_id::create("receive_octets_count_reg");
		receive_octets_count_reg.configure(.blk_parent(this));
		receive_octets_count_reg.build();
		receive_packet_count_reg = Receive_Packet_Count_Reg::type_id::create("receive_packet_count_reg");
		receive_packet_count_reg.configure(.blk_parent(this));
		receive_packet_count_reg.build();

		reg_map = create_map(	.name("reg_map"),
					.base_addr(32'h0000_0000),
					.n_bytes(8),
					.endian(UVM_LITTLE_ENDIAN)
					);

		reg_map.add_reg( .rg(config_reg),			.offset(8'h00), .rights("RW") );
		reg_map.add_reg( .rg(interrupt_pending_reg),		.offset(8'h08), .rights("RW") );
		reg_map.add_reg( .rg(interrupt_status_reg),		.offset(8'h0C), .rights("RW") );
		reg_map.add_reg( .rg(interrupt_mask_reg),		.offset(8'h10), .rights("RW") );
		reg_map.add_reg( .rg(transmit_octets_count_reg),	.offset(8'h80), .rights("RW") );
		reg_map.add_reg( .rg(transmit_packet_count_reg),	.offset(8'h84), .rights("RW") );
		reg_map.add_reg( .rg(receive_octets_count_reg),		.offset(8'h90), .rights("RW") );
		reg_map.add_reg( .rg(receive_packet_count_reg),		.offset(8'h94), .rights("RW") );

		add_hdl_path(.path("top.dut"));
		lock_model();	
	endfunction: build

endclass: xge_mac_reg_block

class xge_mac_reg_adapter extends uvm_reg_adapter;

	`uvm_object_utils(xge_mac_reg_adapter)

	function new(string name="xge_mac_reg_adapter");
		super.new(name);
	endfunction: new

	virtual function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw);
		wb_transaction trans = wb_transaction::type_id::create("trans");
		if(rw.kind == UVM_WRITE) begin
			trans.wb_we		= 1'b1;
			trans.wb_dat_m2s	= rw.data;
		end
		else if(rw.kind == UVM_READ) begin
			trans.wb_we	= 1'b0;
		end
		trans.wb_adr	= rw.addr;
		trans.wb_cyc	= 1'b1;
		trans.wb_stb	= 1'b1;
		//trans.wb_sel		// Non-existent on DUT
		
		return trans;
	endfunction: reg2bus

	virtual function void bus2reg(uvm_sequence_item bus_item, ref uvm_reg_bus_op rw);
		wb_transaction trans;

		if(!$cast(trans, bus_item)) begin
			`uvm_fatal(get_name(), "bus_item is not of type wb_transaction")
			return;
		end

		if(1'b1 == trans.wb_ack) begin
			if(1'b1 == trans.wb_we) begin
				rw.kind = UVM_WRITE;
				rw.data = trans.wb_dat_m2s;
			end
			else if(1'b0 == trans.wb_we) begin
				rw.kind = UVM_READ;
				rw.data = trans.wb_dat_s2m;
			end

			rw.addr		= trans.wb_adr;
			rw.status	= UVM_IS_OK;
		end

	endfunction: bus2reg

endclass: xge_mac_reg_adapter

`endif
