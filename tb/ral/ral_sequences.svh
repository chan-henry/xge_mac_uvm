`ifndef RAL_SEQUENCES_SVH
`define RAL_SEQUENCES_SVH
class xge_mac_reg_seq extends uvm_reg_sequence;

	`uvm_object_utils(xge_mac_reg_seq)

	function new(string name="xge_mac_reg_seq");
		super.new(name);
	endfunction: new

	virtual task body();
		xge_mac_reg_block	reg_block;

		uvm_status_e		status;
		//uvm_reg_data_t		value;
		bit [31:0]		value;

		if(!$cast(reg_block, model)) begin
			`uvm_fatal(get_name(), "Could not cast model to reg_block")
		end

		reg_block.transmit_octets_count_reg.read(status, value, UVM_BACKDOOR, .map(reg_block.reg_map), .parent(this));
		//peek_reg(reg_block.transmit_octets_count_reg, status, value);
		`uvm_info(get_name(), $sformatf("Reading transmit_octets_count_reg: 32'h%0h", value), UVM_LOW)
		reg_block.receive_octets_count_reg.read(status, value, UVM_BACKDOOR, .parent(this));
		//peek_reg(reg_block.receive_octets_count_reg, status, value);
		`uvm_info(get_name(), $sformatf("Reading receive_octets_count_reg: 32'h%0h", value), UVM_LOW)
	endtask: body

endclass: xge_mac_reg_seq
`endif
