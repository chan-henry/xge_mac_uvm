`timescale 1ps/1ps

interface rst_if(input logic clk);

	logic rst;
	logic rst_n;

endinterface: rst_if

interface wishbone(input logic clk, input rst);

	logic	[7:0]	adr;
	logic	[31:0]	dat_m2s;
	logic	[31:0]	dat_s2m;
	logic		we;
	logic		sel;
	logic		stb;
	logic		ack;
	logic		cyc;
	logic		interrupt;

	clocking master_cb @(posedge clk);
		input	dat_s2m, ack, interrupt;
		output	/*clk, rst,*/ adr, dat_m2s, we, sel, stb, cyc;
	endclocking: master_cb

	modport master	(input /*clk, rst,*/ dat_s2m, ack, interrupt, output adr, dat_m2s, we, sel, stb, cyc);
	modport slave	(input /*clk, rst,*/ dat_m2s, adr, we, sel, stb, cyc, output dat_s2m, ack, interrupt);

endinterface: wishbone

interface xgmii	(input tx_clk, input rx_clk);

	logic	tx_reset_n;
	logic	rx_reset_n;

	logic	[7:0]	rxc;
	logic	[63:0]	rxd;

	logic	[7:0]	txc;
	logic	[63:0]	txd;

	clocking cb @(posedge tx_clk);
		input	tx_clk, tx_reset_n, rx_reset_n, txc, txd;
		output	rx_clk, rxc, rxd;
	endclocking: cb

	modport master	(input tx_clk, rx_clk, tx_reset_n, rx_reset_n, rxc, rxd, output txc, txd);
	modport slave	(input tx_clk, rx_clk, tx_reset_n, rx_reset_n, txc, txd, output rxc, rxd);

	covergroup cg_xgmii_tx @(posedge tx_clk);
		option.per_instance = 1;
		coverpoint tx_reset_n;
		coverpoint rx_reset_n;
		coverpoint txd	{	bins sop	= {64'hD5555555555555FB};
					bins idle	= {64'h0707070707070707};
					bins eop	= {64'h07070707070707FD};	// This eop is aligned to specific message length (that's why it's not being hit in coverage)
					bins others	= default;
				}
		coverpoint rxd	{	bins sop	= {64'hD5555555555555FB};
					bins idle	= {64'h0707070707070707};
					bins eop	= {64'h07070707070707FD};	// This eop is aligned to specific message length (that's why it's not being hit in coverage)
					bins others	= default;
				}
		coverpoint txc;
		coverpoint rxc;
	endgroup: cg_xgmii_tx

	cg_xgmii_tx m_cg_xgmii_tx = new();

endinterface: xgmii

interface pkt_tx (input clk_156m25, input pkt_tx_rst_n);

	logic		pkt_tx_val;
	logic		pkt_tx_sop;
	logic	[2:0]	pkt_tx_mod;
	logic		pkt_tx_eop;
	logic	[63:0]	pkt_tx_data;
	logic		pkt_tx_full;

	clocking cb @(posedge clk_156m25);
		input	/*clk_156m25,*/ pkt_tx_rst_n, pkt_tx_full;
		output	pkt_tx_val, pkt_tx_sop, pkt_tx_mod, pkt_tx_eop, pkt_tx_data;
	endclocking: cb

	clocking cbn @(negedge clk_156m25);
		input	/*clk_156m25,*/ pkt_tx_rst_n, pkt_tx_full;
		output	pkt_tx_val, pkt_tx_sop, pkt_tx_mod, pkt_tx_eop, pkt_tx_data;
	endclocking: cbn

	modport master	(input /*clk_156m25,*/ pkt_tx_rst_n, pkt_tx_full, output pkt_tx_val, pkt_tx_sop, pkt_tx_mod, pkt_tx_eop, pkt_tx_data);

endinterface: pkt_tx

interface pkt_rx (input clk_156m25, input pkt_rx_rst_n);

	logic		pkt_rx_val;
	logic		pkt_rx_sop;
	logic	[2:0]	pkt_rx_mod;
	logic		pkt_rx_err;
	logic		pkt_rx_eop;
	logic	[63:0]	pkt_rx_data;
	logic		pkt_rx_avail;
	logic		pkt_rx_ren;

	clocking cb @(posedge clk_156m25);
		input /*clk_156m25,*/ pkt_rx_rst_n, pkt_rx_avail, pkt_rx_data, pkt_rx_eop, pkt_rx_val, pkt_rx_sop, pkt_rx_mod, pkt_rx_err;
		output pkt_rx_ren;
	endclocking: cb

	clocking cbn @(negedge clk_156m25);
		input /*clk_156m25,*/ pkt_rx_rst_n, pkt_rx_avail, pkt_rx_data, pkt_rx_eop, pkt_rx_val, pkt_rx_sop, pkt_rx_mod, pkt_rx_err;
		output pkt_rx_ren;
	endclocking: cbn

	modport master	(input /*clk_156m25,*/ pkt_rx_rst_n, pkt_rx_avail, pkt_rx_data, pkt_rx_eop, pkt_rx_val, pkt_rx_sop, pkt_rx_mod, pkt_rx_err, output pkt_rx_ren);

endinterface: pkt_rx

module top;

	import uvm_pkg::*;
	import xge_mac_pkg::*;

	logic	clk_156m25	= 1'b0;
	logic	clk_xgmii_tx	= 1'b0;
	logic	clk_xgmii_rx	= 1'b0;
	logic	clk_wb		= 1'b0;
	//logic	rst_n		= 1'b0;

	initial begin
		forever #10000 clk_wb = ~clk_wb;	// 50 MHz clock with 20,000 ps period
	end

	initial begin
		forever #3200 clk_156m25 = ~clk_156m25;	// 156.25 MHz clock with 6,400 ps period
	end

	initial begin
		forever #3200 clk_xgmii_tx = ~clk_xgmii_tx; // 156.25 MHz clock with 6,400 ps period
	end

	initial begin
		forever #3200 clk_xgmii_rx = ~clk_xgmii_rx; // 156.25 MHz clock with 6,400 ps period
	end

	//initial begin
	//	//repeat (3) @(negedge clk_156m25);
	//	@(negedge clk_156m25);
	//	rst_n <= 1'b1;
	//end

	rst_if		wb_rst_if	(.clk(clk_wb));
	rst_if		dut_rst_if	(.clk(clk_156m25));
	wishbone	wb_if0		(.clk(clk_wb), .rst(wb_rst_if.rst));
	xgmii		xgmii_if0	(.tx_clk(clk_xgmii_tx), .rx_clk	(clk_xgmii_rx));
	pkt_tx		pkt_tx_if0	(.clk_156m25(clk_156m25), .pkt_tx_rst_n(dut_rst_if.rst_n));
	pkt_rx		pkt_rx_if0	(.clk_156m25(clk_156m25), .pkt_rx_rst_n(dut_rst_if.rst_n));

	xge_mac		dut	(	// Inputs
					.xgmii_rxd	(xgmii_if0.master.rxd	),
					.xgmii_rxc	(xgmii_if0.master.rxc	),
					.wb_we_i	(wb_if0.slave.we	),
					.wb_stb_i	(wb_if0.slave.stb	),
					.wb_rst_i	(wb_rst_if.rst		),
					.wb_dat_i	(wb_if0.slave.dat_m2s	),
					.wb_cyc_i	(wb_if0.slave.cyc	),
					.wb_clk_i	(clk_wb			),
					.wb_adr_i	(wb_if0.master.adr	),
					.reset_xgmii_tx_n	(dut_rst_if.rst_n	),
					.reset_xgmii_rx_n	(dut_rst_if.rst_n	),
					.reset_156m25_n		(dut_rst_if.rst_n	),
					.pkt_tx_val		(pkt_tx_if0.master.pkt_tx_val	),
					.pkt_tx_sop		(pkt_tx_if0.master.pkt_tx_sop	),
					.pkt_tx_mod		(pkt_tx_if0.master.pkt_tx_mod	),
					.pkt_tx_eop		(pkt_tx_if0.master.pkt_tx_eop	),
					.pkt_tx_data		(pkt_tx_if0.master.pkt_tx_data	),
					.pkt_rx_ren		(pkt_rx_if0.master.pkt_rx_ren	),
					.clk_xgmii_tx		(clk_xgmii_tx	),
					.clk_xgmii_rx		(clk_xgmii_rx	),
					.clk_156m25		(clk_156m25	),

							// Outputs
					.xgmii_txd	(xgmii_if0.master.txd		),
					.xgmii_txc	(xgmii_if0.master.txc		),
					.wb_int_o	(wb_if0.slave.interrupt		),
					.wb_dat_o	(wb_if0.slave.dat_s2m		),
					.wb_ack_o	(wb_if0.slave.ack		),
					.pkt_tx_full	(pkt_tx_if0.master.pkt_tx_full	),
					.pkt_rx_val	(pkt_rx_if0.master.pkt_rx_val	),
					.pkt_rx_sop	(pkt_rx_if0.master.pkt_rx_sop	),
					.pkt_rx_mod	(pkt_rx_if0.master.pkt_rx_mod	),
					.pkt_rx_err	(pkt_rx_if0.master.pkt_rx_err	),
					.pkt_rx_eop	(pkt_rx_if0.master.pkt_rx_eop	),
					.pkt_rx_data	(pkt_rx_if0.master.pkt_rx_data	),
					.pkt_rx_avail	(pkt_rx_if0.master.pkt_rx_avail	)
				);

	initial begin
		uvm_config_db#(virtual rst_if)::set(.cntxt(null), .inst_name("uvm_test_top.m_xge_mac_env.m_wb_rst_driver"), .field_name("rst_if"), .value(wb_rst_if));
		uvm_config_db#(virtual rst_if)::set(.cntxt(null), .inst_name("uvm_test_top.m_xge_mac_env.m_rst_driver"), .field_name("rst_if"), .value(dut_rst_if));
		uvm_config_db#(virtual wishbone)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("wb_vif"), .value(wb_if0));
		uvm_config_db#(virtual xgmii)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("xgmii_vif"), .value(xgmii_if0));
		uvm_config_db#(virtual pkt_tx)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("pkt_tx_vif"), .value(pkt_tx_if0));
		uvm_config_db#(virtual pkt_rx)::set(.cntxt(null), .inst_name("uvm_test_top.*"), .field_name("pkt_rx_vif"), .value(pkt_rx_if0));

		run_test();
	end

endmodule: top
