onerror { quit }

vsim	\
-sv_seed_random	\
+UVM_TESTNAME=xge_mac_test	\
+access +r	\
-advdataflow \
-acdb	\
xge_mac.top

log -mem -rec *

run -all

acdb report	\
-html	\
-i xge_mac.acdb	\
-o coverage_report.html

endsim

quit
