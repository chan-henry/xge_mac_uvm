onerror { quit }

vlib xge_mac

vlog	\
-work xge_mac	\
+incdir+"../rtl/include"	\
-coverage sbec	\
-error_limit 1	\
-msg 1	\
-incr	\
../rtl/include/*.v	\
../rtl/verilog/*.v

vlog	\
-work xge_mac	\
-l uvm_1_2	\
+incdir+"$aldec/vlib/uvm-1.2/src"+"../tb/"	\
-coverage sbec	\
-error_limit 1	\
-msg 1	\
../tb/xge_mac_pkg.sv	\
../tb/top.sv

#do sim.do
